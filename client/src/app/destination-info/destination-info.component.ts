import { Component, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DestinationsService } from '../services/destinations.service';
import { DestinationModel } from '../models/destination.model';
import { Subscription } from 'rxjs';
import { HotelModel } from '../models/hotel.model';
import { VillaModel } from '../models/villa.model';
import { ApartmentModel } from '../models/apartment.model';
import { LocationModel } from '../models/location.model';

@Component({
  selector: 'app-destination-info',
  templateUrl: './destination-info.component.html',
  styleUrls: ['./destination-info.component.css']
})
export class DestinationInfoComponent implements OnDestroy {
    public destination: DestinationModel;
    
    private paramMapSub: Subscription = null;
    
    
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private destinationsService: DestinationsService) {
     this.paramMapSub = this.route.paramMap.subscribe(params => {
      const dId = params.get('destinationId');

      this.destinationsService.getDestinationById(dId)
        .subscribe((destination: DestinationModel) => {
            this.destination = destination;
            });
        });
    }
    
  ngOnDestroy() {
    if (this.paramMapSub !== null) {
      this.paramMapSub.unsubscribe();
    }
  }
  
}
