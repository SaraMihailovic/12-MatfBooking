import { Component, OnInit } from '@angular/core';
import { UserService } from "../services/user.service";
import { UserModel } from "../models/user.model";
import { FormControl, FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';
import { Subscription } from "rxjs";
import {catchError, first} from "rxjs/operators";
import {Observable, of} from "rxjs";
import { Router, ActivatedRoute} from '@angular/router';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

   public userForm: FormGroup;
   
   public errors: any[] = [];
   public notifyMessage: string = '';

   private activeSubs: Subscription[]=[];

   constructor(private UserService: UserService, private formBuilder: FormBuilder, private router: Router, private route: ActivatedRoute) { 
    this.userForm = this.formBuilder.group({
      username:['', [Validators.required, Validators.minLength(5), Validators.maxLength(15)]],
      email: ['', [Validators.required, Validators.email]],
       });
    };
  
  ngOnInit(): void {
  
  }

   public currentUser():UserModel{
  	    return this.UserService.currentUser;
  }
  
 public onSubmitUserForm(data){
    
    if(this.userForm.valid)
    {
    console.log(data);
    }
    else
    {
      window.alert('Not valid');
      return;
    }
    
    this.userForm.reset();
   
    const result:Observable<any>= this.UserService.changeUserInfoData(data.username, data.email);
    result.pipe(first(),catchError(err=>{
    window.alert(err.message);
    return of();
})).subscribe(object=>{
    window.alert(object.message);
});

}
    
  public email()
  {
    return this.userForm.get('email');
  }

  public username()
  {
    return this.userForm.get('username');
  }

}
