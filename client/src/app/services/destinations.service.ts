import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { DestinationModel } from '../models/destination.model';
import { HotelModel } from '../models/hotel.model';
import { VillaModel } from '../models/villa.model';
import { ApartmentModel } from '../models/apartment.model';
import { LocationModel } from '../models/location.model';


@Injectable({
  providedIn: 'root'
})
export class DestinationsService {
  private destinations: Observable<DestinationModel[]>;
  
  private readonly destinationsUrl = 'http://localhost:3000/destinations/';
  
  private readonly hotelsUrl = 'http://localhost:3000/hotels/';
  
  private readonly villasUrl = 'http://localhost:3000/villas/';
  
  private readonly apartmentsUrl = 'http://localhost:3000/apartments/';
  
  private readonly locationsUrl = 'http://localhost:3000/locations/';
  
  constructor(private http: HttpClient) {
    this.refreshDestinations();
  }

  private refreshDestinations(): Observable<DestinationModel[]> {
    this.destinations = this.http.get<DestinationModel[]>(this.destinationsUrl);
    return this.destinations; 
  }
  
  public getDestinations(): Observable<DestinationModel[]> {
    return this.destinations;
  }
  
  public getDestinationById(id: string): Observable<DestinationModel> {
    return this.http.get<DestinationModel>(this.destinationsUrl + id);
  }
      
  public getVillas(): Observable<VillaModel[]> {   
    return this.http.get<VillaModel[]>(this.villasUrl);
  }
  
  public getVillaById(id: string): Observable<VillaModel> {
    return this.http.get<VillaModel>(this.villasUrl + id);
  }
  
  public getHotels(): Observable<HotelModel[]> {
     return this.http.get<HotelModel[]>(this.hotelsUrl);
  }

  public getHotelById(id: string): Observable<HotelModel> {
    return this.http.get<HotelModel>(this.hotelsUrl + id);
  }
  
  public getApartments(): Observable<ApartmentModel[]> {
    return this.http.get<ApartmentModel[]>(this.apartmentsUrl);
  }
 
  public getApartmentById(id: string): Observable<ApartmentModel> {
    return this.http.get<ApartmentModel>(this.apartmentsUrl + id);
  }
  
  public getLocations(): Observable<LocationModel[]> {
    return this.http.get<LocationModel[]>(this.locationsUrl);
  }
    
  public getLocationById(id: string): Observable<LocationModel> {
    return this.http.get<LocationModel>(this.locationsUrl + id);
  }
    
}
