import { UserService } from './user.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {Apartment} from '../models/apartment.model';
import { HttpClient} from '@angular/common/http';



@Injectable({
  providedIn: 'root'
})
export class ApartmentService {

  private readonly apartmentsUrl = 'http://localhost:3000/apartments/';
  private authHeaderObject:()=>object = ()=>{
    return {headers:{Authorization:"Bearer "+ localStorage.getItem('jwt')}};
  
   }

  constructor(private http: HttpClient) { }
  
  public getApartmentById(id: string): Observable<Apartment> {
    return this.http.get<Apartment>(this.apartmentsUrl + id);
  }
    
  public getApartments(): Observable<any>{
    return this.http.get(this.apartmentsUrl);
   }

   public addNewApartment(data: object): Observable<Apartment>{
    return this.http.post<Apartment>(this.apartmentsUrl,data,this.authHeaderObject());
  }   

  

}
