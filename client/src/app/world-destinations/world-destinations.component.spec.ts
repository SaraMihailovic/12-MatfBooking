import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WorldDestinationsComponent } from './world-destinations.component';

describe('WorldDestinationsComponent', () => {
  let component: WorldDestinationsComponent;
  let fixture: ComponentFixture<WorldDestinationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WorldDestinationsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WorldDestinationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
