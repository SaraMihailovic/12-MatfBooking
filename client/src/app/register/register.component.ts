import { Component, OnInit } from '@angular/core';
import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms'
import { Validators } from '@angular/forms';
import { FormGroup, FormBuilder } from '@angular/forms';
import { UserService } from '../services/user.service';
import {catchError, first} from "rxjs/operators";
import {Observable, of} from "rxjs";


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  public registerForm: FormGroup;
  
  constructor(private UserService: UserService, private formBuilder: FormBuilder) { 
    this.registerForm = this.formBuilder.group({
      name:['',[Validators.required, Validators.pattern('[A-Z][a-z]+')]], 
      surname:['',[Validators.required,Validators.pattern('[A-Z][a-z]+')]],
      email: ['', [Validators.required, Validators.email]],
      username:['', [Validators.required, Validators.minLength(5), Validators.maxLength(15)]],
      password:['',[Validators.required, Validators.pattern('^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$')]],
      confirmPassword:['', [Validators.required, this.matchPassword()]],
      gender:['male', [Validators.required]],
      userRole:['guest',[Validators.required]]

    });
  }

  ngOnInit(): void {
  }
  
  public submitForm(){
  
     const data  = this.registerForm.value;
    
    if (this.registerForm.valid) {
      //alert('Form Submitted succesfully!!!\n Check the values in browser console.');
      console.log(data);
    }
    else
    {
      window.alert('Not valid');
      return;
    }

    const body = {
      "username": data.username,
      "password": data.password,
      "email": data.email,
      "name": data.name,
      "surname": data.surname,
      "gender": data.gender,
      "userRole": data.userRole
    }
    
    console.log(body);
    
    this.registerForm.reset();
   
    const result:Observable<any>= this.UserService.registerNewUser(body);
    result.pipe(first(),catchError(err=>{
    window.alert(err.message);
    return of();
})).subscribe(object=>{
    window.alert(object.message);
});
}

  public name()
  {
    return this.registerForm.get('name');
  }

  public surname()
  {
    return this.registerForm.get('surname');
  }

  public email()
  {
    return this.registerForm.get('email');
  }

  public username()
  {
    return this.registerForm.get('username');
  }

  public password()
  {
    return this.registerForm.get('password');
  }

  public confirmPassword()
  {
    return this.registerForm.get('confirmPassword');
  }


public matchPassword() : ValidatorFn {
        return (control: AbstractControl): ValidationErrors => {
          if (!this.registerForm)
            return null;
          const passwordConfirmed = control.value === '' ? false : control.value === this.registerForm.get('password').value;
          return passwordConfirmed ? null : { unconfirmedPassword: true };
        }
}

}
