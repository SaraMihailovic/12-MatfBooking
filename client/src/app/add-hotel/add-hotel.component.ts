import { HotelService } from './../services/hotel.service';
import { Router } from '@angular/router';
import { Hotel } from './../models/hotel.model';
import { FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { UserService } from '../services/user.service';
import { UserModel } from '../models/user.model';
import { Observable } from 'rxjs';



@Component({
  selector: 'app-add-hotel',
  templateUrl: './add-hotel.component.html',
  styleUrls: ['./add-hotel.component.css']
})
export class AddHotelComponent implements OnInit {

  newHotel: Hotel;
  errors: any[] = [];
  selectedFile: File =null;

  constructor(private httpClient:HttpClient, private hotelService: HotelService, private userService: UserService, private router: Router) { }

  ngOnInit(): void {
    this.newHotel = new Hotel();
  }

  public currentUser():UserModel{
    return this.userService.currentUser;
  }

  postFile(fileToUpload: File): Observable<any> {
    const endpoint = 'http://localhost:3000/hotels/file'
    const formData: FormData = new FormData();
    formData.append('image', fileToUpload, fileToUpload.name);
    return this.httpClient
      .post(endpoint, formData);
  }
  handleImageUpload(files:FileList){
    this.selectedFile=files.item(0);
    this.postFile(this.selectedFile).subscribe((response) => {
      console.log('response received is ', response);
      this.newHotel.images="../../assets/img/gallery/" + this.selectedFile.name;
  })
  }


  public createHotel(){
    this.hotelService.addNewHotel(this.newHotel).subscribe(
      (rental: Hotel)=>{
        alert('Hotel is successfully created!');
        this.router.navigate([`/`])
      },
      (errorresponse: HttpErrorResponse)=>{
          this.errors = errorresponse.error.errors;
      }
    )
  }


}
