import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AgmCoreModule } from '@agm/core';
import { AppRoutingModule } from './routes/app-routing.module';
import {} from '@google/maps'


import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { SerbiaDestinationsComponent } from './serbia-destinations/serbia-destinations.component';
import { WorldDestinationsComponent } from './world-destinations/world-destinations.component';
import { LoginComponent } from './login/login.component';
import { DestinationInfoComponent } from './destination-info/destination-info.component';
import { MapComponent } from './map/map.component';
import { BookingComponent } from './booking/booking-villa/booking.component';
import { RegisterComponent } from './register/register.component';
import { StaysComponent } from './admin/stays/stays.component';
import { UsersComponent } from './admin/users/users.component';
import { ProfileComponent } from './profile/profile.component';
import { AddApartmentComponent } from './add-apartment/add-apartment.component';
import { AddVillaComponent } from './add-villa/add-villa.component';
import { AddHotelComponent } from './add-hotel/add-hotel.component';
import { BookingHotelComponent } from './booking/booking-hotel/booking-hotel.component';
import { BookingApartmentComponent } from './booking/booking-apartment/booking-apartment.component';
import { MyReservationsComponent } from './my-reservations/my-reservations.component';
import { MyStaysComponent } from './my-stays/my-stays.component';
import { ReservationsComponent } from './reservations/reservations.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    SerbiaDestinationsComponent,
    WorldDestinationsComponent,
    LoginComponent,
    DestinationInfoComponent,
    MapComponent,
    BookingComponent,
    RegisterComponent,
    StaysComponent,
    UsersComponent,
    ProfileComponent,
    AddApartmentComponent,
    AddVillaComponent,
    AddHotelComponent,
    BookingHotelComponent,
    BookingApartmentComponent,
    MyReservationsComponent,
    MyStaysComponent,
    ReservationsComponent,
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    AgmCoreModule.forRoot({
      apiKey: ''
  })
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas:[
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ]
})
export class AppModule { }
