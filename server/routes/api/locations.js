const express=require('express');
const router=express.Router();


const controller = require('../../controllers/locationsController');

//http:://localhost:3000/api/locations
router.get('/', controller.getLocations);

router.get('/:id', controller.getLocationById);

module.exports = router;

