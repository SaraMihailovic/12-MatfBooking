const express=require('express');
const router=express.Router();
const authenticated=require('../../middleware/authenticated');
const multer = require('multer');


const controller = require('../../controllers/hotelsController');

//define storage for the images
const storage = multer.diskStorage({
    //destination for files
    destination: function(request, file, callback){
        callback(null,'./../client/src/assets/img/gallery');
    },

    //addd back the extension
    filename: function(request, file, callback){
        callback(null,file.originalname);
    }
});

//upload parameters from multer
const upload = multer({
    storage:storage,
    limits:{
        fieldSize:1024*1024*3
    },
});

//http:://localhost:3000/api/hotels
router.get('/', controller.getHotels);

//http://localhost:3000/api/hotels/username
router.get('/user/:username',controller.getUsersHotels);

router.get('/:id',controller.getHotelsById);

router.post('/',authenticated, controller.addNewHotel);

router.post('/file', upload.single('image'), controller.addFile);

module.exports = router;

