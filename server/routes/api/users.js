const express=require('express');
const router=express.Router();
const authenticated=require('../../middleware/authenticated')


const controller = require('../../controllers/usersController');

//http:://localhost:3000/api/users
router.get('/', controller.getUsers);

router.get('/currentUser',authenticated ,controller.getCurrentUser);

//http://localhost:3000/api/users/username
router.get('/:id', authenticated, controller.getUserById);

//http://localhost:3000/api/users/username
router.get('/:username', controller.getUserByUsername);

router.post('/', controller.addNewUser);

router.post('/login', controller.login);

router.put('/:username', controller.updateUserPassword);

router.patch('/change-password/:id', authenticated, controller.changeUserInfoData);

router.delete('/:username', controller.deleteUser);

module.exports = router;

