const mongoose = require('mongoose');
const Addresses = require('../models/addresses');

module.exports.getAddresses = async (req, res, next) => {
  try {
    const addresses = await Addresses.find({}).exec();
    res.status(200).json(addresses);
  } catch (err) {
    next(err);
  }
};