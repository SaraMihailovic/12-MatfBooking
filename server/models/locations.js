const mongoose =require('mongoose');

const locationSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    width: {
        type: Number,
        required: true, 
        default: '', 
        trim: true
    },
    height: {
        type: Number,
        required: true,
        default: '', 
        trim: true
    },
    adress: {
        type: String,
        required: true,
        default: '', 
        trim: true
    }
    
});

const Location=mongoose.model('Location',locationSchema);

module.exports=Location;
  
  
