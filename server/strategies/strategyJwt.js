const passport = require('passport');
const {ExtractJwt,Strategy} = require('passport-jwt');

const mongoose = require('mongoose');
const User = require('../models/user');

passport.use(
  
  new Strategy({
  
     jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
     secretOrKey: "5105204425768ee9debc645695618ff5407e4ffe3057d2d5fd",
     
     },async (jwtPayload,done)=>{
     
       try{
        const user=await User.findById(jwtPayload.id).exec();
        if(!user)
          return done(null,false);
        
        return done(null,user);
        
       
       }catch (e)
       {
         return done(e);
       }
  
  
  })



);
